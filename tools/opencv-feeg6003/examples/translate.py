import numpy as np
import cv2 as cv
img = cv.imread('logo.png')
rows, cols = img.shape[:2]
M = np.float32([[1,0,100],[0,1,50]])
image_new = cv.warpAffine(img,M,(cols,rows))
cv.imshow('Translation',image_new)
cv.waitKey(0)
cv.destroyAllWindows()
