SELECT

(SELECT COUNT(DISTINCT Character)
 FROM Characters_Table
 WHERE Character LIKE '%Stark%') AS 'Stark as part of name'

(SELECT COUNT(DISTINCT Character)
 FROM Characters_Table
 WHERE Surname = 'Stark') AS 'True Stark'
