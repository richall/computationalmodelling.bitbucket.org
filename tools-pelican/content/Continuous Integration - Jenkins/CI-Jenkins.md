Title: Continuous Integration - Jenkins
Date: 2017-05-09 14:00
Category: Workshop
Tags: Continuous Integration, Jenkins
Authors: Peter Bartram, Samuel Diserens

## Overview
On Thursday 4th May [Peter Bartram](http://cmg.soton.ac.uk/people/pb1n16/) and [Samuel Diserens](http://cmg.soton.ac.uk/people/sdd1n15/) presented a workshop on the practice of Continuous Integration and the use of the open-source software Jenkins <link>. This workshop aimed to introduce participants to some of the key concepts of continuous integration as well as to provide participants with an entry-level knowledge of Jenkins which would enable them to get started and experiment further on their own.

## Continuous Integration
The idea of Continuous Integration is to continually introduce small incremental changes to a software project while regularly building and testing the code. This is compared to merging large completed features into the build and only testing when the development work is completed.

_“Continuous Integration (CI) is a development practice that requires developers to integrate code into a shared repository several times a day. Each check-in is then verified by an automated build, allowing teams to detect problems early.”_
- ThoughtWorks

The advantages of this are:

- Identify conflicts early when multiple developers are committing to the same codebase.
- Reduces the time taken to find bugs.
	- _Finding a bug in 10 lines of code is much easier than finding one in 1,000._
- Testing can be automated, and scheduled to run e.g. daily to identify regression.  
  
<br>

## Jenkins
Jenkins is one of the most commonly used solutions for continuous integration. It is a web based server tool with the ability to:

- Build code
- Parse for warnings / errors
- Run unit tests 
- Display results
- Generate documentation
- Handle code releases

<center>![Jenkins Server/ Client Model]({attach}./JenkinsModel.jpg)</center>
<center> *A diagram showing the Jenkins server/client model* </center>

## Workshop
The workshop consisted of a set of exercises to complete on a virtual machine (see below). A basic Jenkins server had been set up on the machine and participants were walked through the basics of:

- Accessing the server
- Installing plugins
- Creating a new build project and linking to an existing git repository
- Executing simple scripts within the job
- Building on a GiT commit
- Building at time intervals
- Running and fixing unit testing  

  
<br>

## Content
- [*Virtual Machine*](http://www.southampton.ac.uk/~ngcmbits/virtualmachines/)
- [*Lecture Slides*](https://bitbucket.org/computationalmodelling/computationalmodelling.bitbucket.org/raw/87e5251ce2866c414fa43b7f408a9344a0282fbe/tools/Continuous%20Integration%20-%20Jenkins/Continuous%20Integration.pdf)
- [*Practical Tasks*](https://bitbucket.org/computationalmodelling/computationalmodelling.bitbucket.org/raw/87e5251ce2866c414fa43b7f408a9344a0282fbe/tools/Continuous%20Integration%20-%20Jenkins/Practical%20Solutions.pdf)
- [*Practical Solutions*](https://bitbucket.org/computationalmodelling/computationalmodelling.bitbucket.org/raw/87e5251ce2866c414fa43b7f408a9344a0282fbe/tools/Continuous%20Integration%20-%20Jenkins/Practical%20Workshop.pdf)