# TASK1 - Managing Images
#Load opencv library
import cv2 
import numpy as nump

#1-Load an color image. The filename provided is: 'atlantis.jpg'.
colorimg = cv2.imread('atlantis.jpg',1)

#2-Load an image in grayscale mode. The filename is the same as in 
# the previous task.
grayimg = cv2.imread('atlantis.jpg',0)

#3-Display the color image using the specific function for it.
cv2.imshow('Color',colorimg)

#4-Display the grayscale image aforeloaded.
cv2.imshow('Gray',grayimg)

#5-Save the grayscale image into a file named: 'grayimage.jpg'.
cv2.imwrite('grayimage.jpg',grayimg)

# Extra code provided
# Wait for a key stroke
cv2.waitKey(0)
# After the key stroke destroy all windows
cv2.destroyAllWindows()
