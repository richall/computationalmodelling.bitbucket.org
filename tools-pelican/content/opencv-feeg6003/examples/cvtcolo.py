import cv2 as cv
image = cv.imread('logo.png')
image_HSV = cv.cvtColor(image,cv.COLOR_BGR2HSV)
image_Gray = cv.cvtColor(image,cv.COLOR_BGR2GRAY)
cv.imshow('BGR',image)
cv.imshow('HSV',image_HSV)
cv.imshow('Gray',image_Gray)
cv.waitKey(0)
cv.destroyAllWindows()
