title: Fast Prototyping of Neural Networks with Keras
authors: Martin Denyer
date: 2018-05-8
tags: keras, NGCM, VirtualBox, machine-learning, neural-network

<p align="center"><img src="{filename}/keras/keras-logo.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

# Keras

Keras is a high-level neural network API for Python. Keras was built with a focus on enabling fast experimentation, allowing users to quickly build and test neural network models. Although we only touch on Sequential neural networks in this workshop, Keras supports the all widely-used neural network structures, inlcluding recurrent and convolutionalmodels.

# Our Aim

This workshop aims to introduce the fundamental tools necessary to build working neural network models with Keras. Building neural networks requires a basic understanding of them, how they work, and what can be achieved with them; we will touch on this lightly but a detailed understanding is beyond the scope of this course. There is a wealth of tutorials and literature freely availalbe online for readers that are interested to learn more.

# Prerequisites

No prior knowledge of machine learning alogrithms or neural networks will be needed; all required background knowledge will be included in the workshop. This workshop is includes hands-on tutorials that will be performed in Python. Although some prior knowledge of Python would be useful, the user only needs basic programming knowledge to complete the exercises ahead.

# Accessing the virtual machine and Python IDE

The accompanying virtual machine to this workshop can be downloaded [here](https://www.southampton.ac.uk/~ngcmbits/virtualmachines/). For first time users of virtual machines, detailed guidance on how to install the machine image with VirtualBox can be found [here]({filename}/virtualbox-basics/virtualbox-basics.rst).

After installing, access the desktop with the following login details:
* Username: feeg6003
* Password: feeg6003

Keras works with Python, benefiting from its compact, easy to debug code. This workshop will use the Spyder IDE, which has been built within a special conda environemnt for Keras. More information on Anaconda and the spyder IDE can be found [here](https://www.anaconda.org).

To open spyder, perform the following commands:
1. start menu > system tools > LXTerminal
2. feeg6003@ubuntu17:~$ conda activate Keras
3. feeg6003@ubuntu17:~$ spyder

# Neural Networks: Some Background

In this workshop we're going to be building neural network models. A neural network is essentially some black-box funtion that maps a set of numerical inputs to a set of numerical outputs. The function the network represents could be anything, from a model of how property characteristics effect their price, to a model that predicts the severity of patient diabetes given some bloodwork information. The key concept is that the user doesn't need to define the model (i.e. how the black-box function maps our housing characteristics to its predicted price). The neural network instead learns through a process known as training where it builds a representation of the unknown function by learning from a large labelled dataset. In our housing price example, this dataset might be historic house prices and the corresponding property information (e.g. number of bedrooms, number of bathrooms, etc). The power of a neural network is in its ability to perform accurate predictions on unseen data. For example, after training our model on a dataset containing historical housing prices, we want the model to be able to predict the values of new properties given their characteristics.

An understanding of neural networks is best achieved through an example.

# Keras Neural Network: Single layer perceptron

Before we go to several layers network, you will be going through a simple example of a single layer perceptron. The dataset will include four points and four corresponding target values. These datapoints X have two features. Let's call them X1 and X2. The target value will be called y.

~~~python
X = np.array([[0, 0], 
			  [1, 0], 
			  [0, 1], 
			  [1, 1]])

y = np.array([[0], [1], [1], [0]])
~~~

For a visual preception you can see the following image.
<p align="center"><img src="{filename}/keras/and_image.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

The basic idea of this task is to learn the information X to predict y. "H" will be the predicted value. You may adjust x1 and x2 multiplying them by particular weights w1 and w2 and adding a bias b.

<p align="center"><img src="{filename}/keras/and_weights.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

Speaking mathematically you end up with the following equation:

H = w1 * x1 + w2 * x2

Now assume you have the following coefficients 1.41, 1.27 and -2.22 for w1, w2, and b. Therefore, after you plug the data points into the equation above, you will have these results: -2.22, -0.95, -0.8, and 0.46.

As you can see that the first three results are negative and the last one is positive. Now you can use a logical expression like the one below, to classify your observations.

~~~python
if H > 0:
	H = 1
else:
	H = 0
~~~

Although, on the other hand, instead of using the logical expression, you can mimick the similar behaviour by modifying the neuron output so that it is equal to 0 or 1. One of such functions is Sigmoid function:

<p align="center"><img src="{filename}/keras/sigmoid.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

This will give you a modified equation like:

H = sigmoid(w1 * X1 + w2 * X2)

Thefore the neuron output you will have is [0, 0, 0, 1].

<p align="center"><img src="{filename}/keras/and_sigmoid.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

Now, once we have convinced ourselves that this single layer perceptron works well, let's try to use Keras to do the same thing in a programmatically.

# Keras Neural Network: Single layer perceptron with a pre-trained Keras model

First of all you need to import the following libraries.

~~~python
import numpy as np
import keras.models
~~~

You should initialise the data set as following. 

~~~python
import numpy as np
import keras.models

X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
# Y = ?
~~~

This dataset will be used to predict the classes by a pre-trained model.

To import the model we should use the following command: keras.models.load_model(“ANN_and”), where ANN_and is the filename (you can find in the folder).

~~~python
import numpy as np
import keras.models

X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
# Y = ?

model = keras.models.load_model('ANN_and')
~~~

To check the weights of the model we can use functions: model.get_weights() for coefficients, and model.layers[0].activation to see activation function.

~~~python
import numpy as np
import keras.models

X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
# Y = ?

model = keras.models.load_model('ANN_and')
print("\n\nModel Weights: ", model.get_weights(), 
     model.layers[0].activation)
~~~

This should give you the following result, stating that the weights are 1.41 and 1.27, bias is -2.2, and the activation function is sigmoid. All as you have in the example above.
~~~python
#  Output:
#  Model Weights:  [array([[1.416884 ],
#                  [1.2738801]], dtype=float32), 
#			       array([-2.218], dtype=float32)]
# Model Activation <function sigmoid at 0x116c989d8>
~~~

Now, knowing that the model is exactly what you examined above, let’s predict the classes.

To do so, you need to use print(model.predict_classes(X)).

~~~python
import numpy as np
import keras.models


X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
# Y = ?
model = keras.models.load_model('ANN_and')
print(model.predict_classes(X))

# Output:  [[0], [0], [0], [1]]
~~~

As you can see, the model predicts classes correctly.

To sum everything above, we used three functions for the predicting purposes:

~~~python
import keras.models
model = keras.models.load_model('ANN_and)
model.predict_classes(X)
~~~

# Keras Neural Network: Training own model

Now, let’s try to train your model. To do so you will need to import the following modules.
~~~python
from keras.models import Sequential
from keras.layers import Dense
~~~

You need to use Sequential() model with one fully connected layer - means all nodes are connected with the all nodes of the next layer. To add the layer, we should use function model.add(Dense(units=1, input_dim=2, activation='sigmoid')). Dense shows here a fully connected layer, with one unit/neuron, that receives two inputs: X1 and X2. The result is activated with Sigmoid function.

~~~python
from keras.models import Sequential
from keras.layers import Dense

X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
y = np.array([[0], [0], [0], [1]])

model = Sequential()
model.add(Dense(units=1, input_dim=2, activation='sigmoid'))
~~~

This is how you designed your model. Note, that you have not trained the model yet - you just designed the structure of it. Finally, you need to compile the model specifying the learning parameters: MSE loss function, stochastic gradient descent with learning rate of 0.5.

~~~python
model.compile(loss='mean_squared_error',
                optimizer=optimizers.SGD(lr=0.5),
                metrics=[metrics.binary_accuracy])
~~~

To print the summary you can use function print(mode.summary()), which should give you a results like:

<p align="center"><img src="{filename}/keras/slp_summary.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

Now, when you know that the model is what you expected it to be, you can start training it. To train the model you need to use model.fit(X, y), passing data points and target values. This function randomly assigns the initial weights and with learning every data adjusts them.

~~~python
model.fit(X, y)
~~~

Running the function you can also see the expected accuracy rate of the model. Since data set is small and the learning rate above was small you may end up with a 50% accurate model. One way of solving the issue is to increase the number of epochs - which is kind of "reusing" of the data several times and thus, showing Positives and Negatives not just once. So if yo use 75 epochs, then the accuracy should be 100%.

~~~python
model.fit(X, y, epochs=75)
~~~

<p align="center"><img src="{filename}/keras/and_100_accuracy.png" align="right" style="margin: 2em 5em 2em 2em"/></p>




# Keras Neural Network: Logical XOR

We're now going to build a neural network to perform the logical XOR operation. You will need to open the ANN\_logical\_xor.py to work along with this exercise.

As per the logical AND example, the template already includes the data you'll be working with and all the necessary libraries have been imported. The dataset 'X' is unchanged from the previous problem but the corresponding labels 'y' have been updated for the XOR problem.

~~~python
# 0. import libraries
import numpy as np
import tensorflow
from keras.models import Sequential
from keras.layers import Dense
from keras import metrics, optimizers

# 1. create our training data - there are only four possible scenarios
X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
y = np.array([[0], [1], [1], [0]])
~~~

Our previous logical AND model won't work here because the problem isn't linearly seperable (see the figure below).

<p align="center"><img src="{filename}/keras/linear-seperable.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

A single layer with a single unit can only solve linearly seperable problems; to solve more complex problems like logical XOR we'll need to add more layers or units. Keras's abstractions makes adding more units simple to do. In the logical AND model, our model structure looked like this:

~~~python
model = Sequential()
model.add(Dense(units = 1, activation = 'sigmoid', input_dims = 2))
~~~

We can easily add another layer by inserting another 'model.add()' line of code between the two lines of code in the above block:

~~~python
# 2. create our neural network model
model = Sequential()
model.add(Dense(units = 2, activation = 'relu', input_dims = 2))
model.add(Dense(units = 1, activation = 'sigmoid'))
~~~

This demonstrates one of the key 'ease of use' features of Keras. No matter the number of units in each layer, we don't need to tell Keras how to deal with how information is passed from one layer to the next - Keras does the hard work for us! All we need to do is make sure that the 'input\_dims' in the first layer is equal to the dimensions of our samples contained in 'X' and make sure that the number of units in our output layer is equal to the dimensions of our sample labels 'y'. In the above example we choose 2 units for our first layer but you could set this to whatever number you desire (the number of units can even be higher than the number of inputs).

You'll also notice that we've introduced a new activation function for our first layer called 'relu'. The sigmoid function is a good activation function for our output layer because is squashes output between 0 and 1 so that the result can be interpreted as a probability. This isn't necessary for our hidden layers and so we can use a non-saturating function like relu instead. Relu has shown to work better for neural networks hidden layers in practice. A graph of the relu function is included below:

<p align="center"><img src="{filename}/keras/relu.png" align="right" style="margin: 2em 5em 2em 2em"/></p>

Keras has many other activation functions to use, which may be more appropriate depending on the objective of your neural network.

Adding more layers to our XOR model is simple - just keep inserting 'model.add()' layers before the output layer. Remember that you don't need to give the 'input\_dims' variable for layers after the first as in the example below:

~~~python
# 2. create our neural network model
model = Sequential()
model.add(Dense(units = 2, activation = 'relu', input_dims = 2))
model.add(Dense(units = 4, activation = 'relu'))
model.add(Dense(units = 4, activation = 'relu'))
model.add(Dense(units = 1, activation = 'sigmoid'))
~~~


Now we've learnt how to add more layers, lets compile the model and then train it. Compilation is performed in the same manner as in the logical AND exercise:

~~~python
# 3. compile the model (i.e. configure the learning process)
model.compile(loss = 'binary_crossentropy', optimizer = 'sgd', metrics = [metrics.binary_accuracy])
~~~

We've made one change vs the logical AND compilation process. We've switched from using 'mean\_squared\_error' to 'binary\_crossentropy' as our loss function. 'binary\_crossentropy' typically performs better than MSE for binary neural networks but we've used MSE previously as most people are more likely to have seen MSE.

The next step is to fit the model to our training data. Because we're using more layers and nodes, we'll need to perform more passes (epochs) of the training data. This is becuase the network has more internal parameters to adjust so the process of doing so will be longer.

~~~python
# 4. train the model with our datasets and print accuracy
model.fit(X, y, epochs = 1000, verbose = False)
loss, acc = model.evaluate(X, y, verbose = False)
print('\nTraining loss = {:.3f}, accuracy = {}'.format(loss, acc))
~~~

We've chosen 100 epochs for this example but the number of epochs necessary to get satisfactory performance will depend on the complexity of your model (i.e. the number of layers and nodes). The full python script should now look something like this:

~~~python
# 0. import libraries
import numpy as np
import tensorflow
from keras.models import Sequential
from keras.layers import Dense
from keras import metrics, optimizers

# 1. create our training data - there are only four possible scenarios
X = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
y = np.array([[0], [1], [1], [0]])

# 2. create our neural network model
model = Sequential()
model.add(Dense(units = 8, activation = 'relu', input_dim = 2))
model.add(Dense(units = 1, activation = 'sigmoid'))

# 3. compile the model (i.e. configure the learning process)
model.compile(loss = 'mean_squared_error', \
              optimizer = 'sgd', \
              metrics = [metrics.binary_accuracy])

# 4. train the model with our datasets and print accuracy
model.fit(X, y, epochs = 1000, verbose = False)
loss, acc = model.evaluate(X, y, verbose = False)
print('\nTraining loss = {:.3f}, accuracy = {}'.format(loss, acc))

# 5. print our predictions
predictions = model.predict(X)
print('\nModel predictions')
print('-----------------')
for i, prediction in enumerate(predictions):
    print('{} XOR {} = {:.0f}'.format(X[i, 0], X[i, 1], prediction[0]))
~~~

And after executing your code you should get some output similar to this:

~~~python
Training loss = 0.165, accuracy = 1.0

Model predictions
-----------------
0 XOR 0 = 0
1 XOR 0 = 1
0 XOR 1 = 1
1 XOR 1 = 0
~~~

If your accuracy isn't as good, try changing the number of layers and nodes or increasing the number of epochs. You can also try experimenting with other activation functions in the hidden layers or different loss functions in the optimiser.

# Keras neural network: Sequential image recongnition

Now we understand the basics of how to build, compile and train a neural network using Keras, we'll move onto a slightly more difficult problem.

In this problem, we are going to train a neural network to recognise handwritten digits. The neural network we create will be trained on a large set of images where each image contains a single digit in the range 0 to 9. As previously stated, neural networks require numerical inputs, so the image data is stored in two-dimensional 8 by 8 numpy arrays. Each elements of the array represents the brightness of a pixel, with 1 being white and 0 being black.

The Scikit-learn library already contains the normalised image data and corresponding labels that we'll be using for this problem. Open the template python file 'ANN\_digits\_sequential.py'.

The first step is to import the data. Do this by entering the following code into the template:

~~~python
# 1. create our training data
digits = datasets.load_digits()
X = digits.images
y = digits.target
~~~

The object images is a data structure. We seperate out the data and its labels into the numpy arrays 'X' and 'y' respectively. Our sequential network is expecting one-dimensional samples whilst our data stored in 'X' is two-dimensional (you can check this with np.shape(X)). We'll have to use np.reshape() command to flatten our data. We can do this as follows:

~~~python
# 2. data-preprocessing - flatten our images into 1d arrays
X_flattened = X.reshape((np.shape(X)[0], np.shape(X)[1] * np.shape(X)[2]))
~~~

The above commands reduce the 1797 sample into 64 element, one-dimensional arrays.

Unlike our previous examples, our neural network will require 10 nodes in it's output layer. Each of the output nodes will correspond to one of the possible outcomes (the digits 0 to 9). We will interpret the node with the highest response as the prediction of the network. For instance, if the first node's output for a given sample is 0.9, whilst the other 9 nodes outputs are 0.1, the network has predicted that the sample image contains the digit 0.

Our labels are currently stored as a list of digits in the range 0 to 9. We need to convert each digit to a 10-element binary array where the array contains all zeros apart for the element corresponding to the digit the image represents. For instance, if an image has a corresponding label y = 4, we need to convert 4 to y = [0, 0, 0, 0, 1, 0, 0, 0, 0, 0]. Keras includes utilities to make this easy for us. Enter the following code in your template:

~~~python
X_flattened = X.reshape((np.shape(X)[0], np.shape(X)[1] * np.shape(X)[2]))
y_dummy = np_utils.to_categorical(y)
~~~

You can check some of the new 'y\_dummy' variables to make sure that they are correct given their corresponding 'y' value. Do this by highlightin the above code and hitting F9 to execute it. Then, in the terminal try np.shape(y_dummy[#]) where # is some value in the range (0, 1798). You should see arrays in the desired format.

We've done all the pre-processing now so lets move onto building the model. We'll need more layers than our previous AND and XOR models had becuase this is a more complex problem (our sample arrays have more dimensions). Lets try the following model structure:

~~~python
# 3. create our neural network model
model = Sequential()
model.add(Dense(units = 64, activation = 'relu', input_dim = 64))
model.add(Dense(units = 16, activation = 'relu'))
model.add(Dense(units = 16, activation = 'relu'))
model.add(Dense(units = 10, activation = 'softmax'))
~~~

There are a few changes to our model. We're using 'softmax' as our activation function in our output layer (remember that softmax is good for non-binary classification whilst 'sigmoid' is good for binary classification). We're also using relu function in our hidden layers as per our XOR example.

Now lets move onto defining the learning process using the 'compile' method. We compile the model as follows:

~~~python
# 4. compile the model (i.e. configure the learning process)
model.compile(loss = 'mean_squared_error',
		optimizer = 'sgd', metrics = ['accuracy'])
~~~

We're using similar settings to our previous example. You're now ready to execute the code. Run it in your spyder by hitting F5. After execution, you should get some output that shows four images and their corresponding true label and prediction. The python terminal will also output the loss and accuracy of this network.

# That's it!

You've learnt how to build simple neural network models using sequential models. If you'd like to learn more, there are additional examples of the neural networks in the solutions folder, or you can search for the many freely available Keras models online. 

Thanks for your time.


